import UIKit
import Alamofire
import SwiftyJSON

struct SourceItem {
    var name = ""
    var description = ""
    var url = ""
}

class SourceListTableViewController: UITableViewController {
    
    var sourceItems:[SourceItem] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "SOURCES"
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissView))

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        callSourceAPI { (success) in
            if success
            {
                self.tableView.reloadData()
            }
            else
            {
                let alert = UIAlertController(title: "Alert", message: "News Call failed", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)

            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.sourceItems.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sourceCell", for: indexPath)
        let titlelabel = cell.viewWithTag(100) as! UILabel
        let descriptionLabel = cell.viewWithTag(101) as! UILabel
        
        let sourceItem = sourceItems[indexPath.row]
        
        titlelabel.text = sourceItem.name
        descriptionLabel.text = sourceItem.description
        
        cell.accessoryType = .disclosureIndicator

        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "detailVC")
        let webView = detailVC?.view.viewWithTag(100) as! UIWebView
        
        webView.loadRequest(URLRequest(url: URL(string: sourceItems[indexPath.row].url)!))
        self.navigationController?.pushViewController(detailVC!, animated: true)
    }
    
    // MARK: - Helper functions
    
    func callSourceAPI(callback: @escaping(_ success:Bool)->())
    {
        Alamofire.request("https://newsapi.org/v1/sources?language=en&category=technology") //given in assignment
        .validate()
        .responseJSON { (response) in
            
            if response.result.isSuccess
            {
                let json = JSON(response.result.value!)
                self.sourceItems.removeAll()
                for source in json["sources"].arrayValue
                {
                    var sourceItem = SourceItem()
                    sourceItem.name = source["name"].stringValue
                    sourceItem.description = source["description"].stringValue
                    sourceItem.url = source["url"].stringValue
                    
                    self.sourceItems.append(sourceItem)
                }
                callback(true)
            }
            else
            {
                print(response.result.error!)
                callback(false)
            }
        }
    }
    
    func dismissView()
    {
        dismiss(animated: true, completion: nil)
    }

}
