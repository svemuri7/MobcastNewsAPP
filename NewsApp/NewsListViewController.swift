import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage

//News Item model
struct NewsItem{
    var author = ""
    var title = ""
    var description = ""
    var detailUrl = ""
    var imageUrl = ""
    var publishedAt = Date()
}


class NewsListViewController: UIViewController {
    
    @IBOutlet weak var newsTableView:UITableView!
    @IBOutlet weak var dateLabel:UILabel!
    @IBOutlet weak var activityIndicator:UIActivityIndicatorView!
    
    var newsItems:[NewsItem] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.title = "NEWS"
        activityIndicator.center = self.view.center
        self.view.addSubview(self.activityIndicator!)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(didTapSourcesButton))
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        //show date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, dd MMM, yyyy"
        self.dateLabel.text = dateFormatter.string(from: Date())
        
        callNewsAPI { (success) in
            
            if success
            {
                self.activityIndicator?.stopAnimating()
                self.newsTableView.reloadData()
            }
            else
            {
                let alert = UIAlertController(title: "Alert", message: "News Call failed", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func callNewsAPI(callback: @escaping (_ success:Bool)->())
    {
        activityIndicator?.startAnimating()
        Alamofire.request("https://newsapi.org/v1/articles?source=techcrunch&sortBy=latest&apiKey=91c44c7397aa447081d8364656f3ef6e") //given in assignment
        .validate()
        .responseJSON { (response) in
            
            if response.result.isSuccess
            {
                let json = JSON(response.result.value!)
                self.newsItems.removeAll()
                for item in json["articles"].arrayValue
                {
                    var newsItem = NewsItem()
                    newsItem.author = item["author"].stringValue
                    newsItem.title = item["title"].stringValue
                    newsItem.description = item["description"].stringValue
                    newsItem.imageUrl = item["urlToImage"].stringValue
                    newsItem.detailUrl = item["url"].stringValue
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
                    
                    if let publishTime = item["publishedAt"].string
                    {
                        newsItem.publishedAt = dateFormatter.date(from: publishTime)!
                    }
                    
                    self.newsItems.append(newsItem)
                }
                callback(true)
            }
            else
            {
                print(response.result.error!)
                callback(false)
            }
        }
    }
    
    func didTapSourcesButton()
    {
        let sourcesViewController = self.storyboard?.instantiateViewController(withIdentifier: "sourcesVC")
        
        self.present(sourcesViewController!, animated: true, completion: nil)
        
    }

}

extension NewsListViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100 //constant
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newsItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let newsCell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath)
        
        let imageView = newsCell.viewWithTag(100) as! UIImageView
        let titleLabel = newsCell.viewWithTag(101) as! UILabel
        let dateLabel = newsCell.viewWithTag(102) as! UILabel
        let descriptionLabel = newsCell.viewWithTag(103) as! UILabel
        
        let newsItem = newsItems[indexPath.row]
        
        titleLabel.text = newsItem.title
        descriptionLabel.text = newsItem.description
        dateLabel.text = calculateDate(date: newsItem.publishedAt)

        
        //showing image
        let imageViewFrame = imageView.frame
        let loadingLabel = UILabel(frame: CGRect(x: imageViewFrame.minX + 2, y: imageViewFrame.size.height/2, width: imageViewFrame.maxX - 2, height: 20))
        loadingLabel.text = "Loading"
        loadingLabel.textColor = UIColor.lightGray
        imageView.addSubview(loadingLabel)
        
        Alamofire.request(newsItem.imageUrl).responseImage(completionHandler: { (response) in
            
            loadingLabel.removeFromSuperview()
            imageView.image = response.result.value?.af_imageScaled(to: imageView.frame.size)
        })
        
        return newsCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "detailVC")
        let webView = detailVC?.view.viewWithTag(100) as! UIWebView
        
        webView.loadRequest(URLRequest(url: URL(string: newsItems[indexPath.row].detailUrl)!))
        self.navigationController?.pushViewController(detailVC!, animated: true)
        
    }
    
    func calculateDate(date:Date) -> String
    {
        //With the Date class introduced in Swift 3, dates are directly comparable without the use of compare function unlike NSDate.
        //This code can be further simplified.
        
        let todaysDate = Date()
        let todayCalendar = NSCalendar.current
        let todayComponents = todayCalendar.dateComponents([.hour,.minute,.day,.month,.year], from: todaysDate as Date)
        guard let nowHour = todayComponents.hour else{ return ""}
        guard let nowMinutes = todayComponents.minute else{ return ""}
        guard let nowDay = todayComponents.day else{ return ""}
        guard let nowMonth = todayComponents.month else{ return ""}
        guard let nowYear = todayComponents.year else{ return ""}
        
        
        let loggedDate = date
        let calendar = NSCalendar.current
        let components = calendar.dateComponents([.hour,.minute,.day,.month,.year], from: loggedDate as Date)
        guard let hour = components.hour else{ return ""}
        guard let minutes = components.minute else{ return ""}
        guard let day = components.day else{ return ""}
        guard let month = components.month else{ return ""}
        guard let year = components.year else{ return ""}
        
        if year == nowYear && month == nowMonth
        {
            if day == nowDay
            {
                if hour == nowHour
                {
                    if minutes == nowMinutes
                    {
                        return "Now"
                    }
                    else
                    {
                        if nowMinutes-minutes <= 1 {return "1 minute ago"} else {return "\(nowMinutes-minutes) minutes ago"}
                    }
                }
                else
                {
                    if nowMinutes-minutes == 1 {return "1 hour ago"} else {return "\(nowHour-hour) hours ago"}
                }
            }
            else
            {
                if nowDay-day == 1 {return "1 day ago"} else {return "\(nowDay-day) days ago"}
            }
            
        }
        else
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            return dateFormatter.string(from: date as Date)
            
        }

    }

}

